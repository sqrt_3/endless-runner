﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlatformerCharacter2D : MonoBehaviour
{
    private const string SpeedParam = "Speed";
    private const string GroundParam = "Ground";
    private const float GroundedRadius = .2f;   // Radius of the overlap circle to determine if grounded
    
    [SerializeField]  
    private float maxSpeed = 10f;   // The fastest the player can travel in the x axis
    [SerializeField]
    private float jumpForce = 400f; 
    [SerializeField]
    private bool airControl;        // Whether or not a player can steer while jumping
    [SerializeField]
    private Transform groundCheck;
    [SerializeField]
    private LayerMask groundLayers;           

    private bool grounded; 
    private bool facingRight = true;
    private float killY = -12f;

    private Animator anim;
    private Rigidbody2D rb2D;
    private PlayerHealth health;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        health = GetComponent<PlayerHealth>();

        if (!groundCheck)
        {
            Debug.LogError("Ground Check Transform has not been asssigned");
        }
    }

    private void FixedUpdate()
    {
        grounded = false;

        if (transform.position.y <= killY)
            health.OnPlayerDeath();

        if(groundCheck)
        {
            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, GroundedRadius, groundLayers);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                    grounded = true;
            }
        }

        anim.SetBool(GroundParam, grounded);
    }

    public void Move(float move, bool jump, bool airJump)
    {
        //Basic movement/texture flipping
        if (grounded || airControl)
        {
            anim.SetFloat(SpeedParam, Mathf.Abs(move));

            rb2D.velocity = new Vector2(move * maxSpeed, rb2D.velocity.y);

            if (move > 0 && !facingRight)
            {
                Flip();
            }
            else if (move < 0 && facingRight)
            {
                Flip();
            }
        }

        //Single jump code
        if (grounded && jump && anim.GetBool(GroundParam))
        {
            grounded = false;
            anim.SetBool(GroundParam, false);
            rb2D.AddForce(new Vector2(0f, jumpForce));
        }
        
        //Double jump code
        if(!grounded && airJump)
        {
            grounded = false;
            anim.SetBool(GroundParam, false);
            rb2D.AddForce(new Vector2(0f, jumpForce / 2f));
        }
        
    }

    private void Flip()
    {
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public bool isGrounded()
    {
        return grounded;
    }

    public bool isFacingRight()
    {
        return facingRight;
    }
}
