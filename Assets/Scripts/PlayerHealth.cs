﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

    private Rigidbody2D rb2D;
    public Transform camera;
    public Color alive;
    public Color hit;
    public Color dead;
    public float startingHealth = 3f;
    private float currentHealth;
    private float timeToDamage = 0f;
    public Image[] healthGUIs;
    public Sprite noHealth;
    public Sprite health;
    public Image gameOver;
    private Platformer2DUserControl movement;
    public Text headshots;
    public Text pushes;
    private int headshotCount = 0;
    private int pushCount = 0;
    private int secondsPassed = 0;
    private int minutesPassed = 0;
    private bool timerRunning = false;
    private float horizontalHit = 1f;
    private float verticalHit = 1f;
    private float shakeCameraRot = 2.7f;

    public Text time;


    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        movement = GetComponent<Platformer2DUserControl>();
        currentHealth = startingHealth;
        UpdateScoreGUI();
        UpdateHealthGUI();
        UpdateTimeGUI();
        gameOver.enabled = false;
    }

    private void FixedUpdate()
    {
        timeToDamage -= Time.deltaTime;
        if(!timerRunning)
        {
            StartCoroutine(TimeCount());
            timerRunning = true;
        }
            
        
    }

    public void OnPlayerDamage()
    {
        if(timeToDamage <= 0.0f)
        {
            Debug.Log("Player took damage!");
            --currentHealth;
            // screen shake/ image thingy / move back
            float x = (movement.character.isFacingRight()) ? -1f : 1f;
            rb2D.MovePosition(new Vector2(rb2D.position.x + x, rb2D.position.y + verticalHit));
            StartCoroutine(CameraShake());
            UpdateHealthGUI();
            timeToDamage = 3.0f;
            if (currentHealth == 0)
                OnPlayerDeath();
            
        }
    }

    public void UpdateHealthGUI()
    {
        for(int i=(int)startingHealth; i > currentHealth; --i)
            healthGUIs[i - 1].sprite = noHealth;

        for (int i = 0; i < currentHealth; ++i)
            healthGUIs[i].sprite = health;
    }

    public void OnPlayerDeath()
    {
        Debug.Log("Player died!");
        gameOver.enabled = true;
        movement.enabled = false;
        Camera cam = camera.GetComponentInChildren<Camera>();
        cam.backgroundColor = dead;
        StartCoroutine(ReloadGame());
    }

    private IEnumerator ReloadGame()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Game");
    }

    public void OnPlayerHeadshot()
    {
        Debug.Log("Headshot!");
        ++headshotCount;
        UpdateScoreGUI();
    }

    public void OnPlayerPush()
    {
        Debug.Log("Push!");
        ++pushCount;
        UpdateScoreGUI();
    }

    public void UpdateScoreGUI()
    {
        headshots.text = headshotCount.ToString();
        pushes.text = pushCount.ToString();
    }

    private void UpdateTimeGUI()
    {
        //adjust for minutes
        if(secondsPassed / 60 > 0)
        {
            minutesPassed = secondsPassed / 60;
            secondsPassed %= 60;
        }
        time.text = minutesPassed.ToString("D2") + ":" + secondsPassed.ToString("D2");
    }

    private IEnumerator TimeCount()
    {
        yield return new WaitForSeconds(1);
        ++secondsPassed;
        UpdateTimeGUI();
        timerRunning = false;
    }

    private IEnumerator CameraShake()
    {
        camera.Rotate(new Vector3(0f, 0f, shakeCameraRot));
        Camera cam = camera.GetComponentInChildren<Camera>();
        cam.backgroundColor = hit;
        yield return new WaitForSeconds(0.2f);
        camera.Rotate(new Vector3(0f, 0f, -shakeCameraRot));
        yield return new WaitForSeconds(0.2f);
        camera.Rotate(new Vector3(0f, 0f, 0f));
        cam.backgroundColor = alive;
    }
}
