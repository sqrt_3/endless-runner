﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(PlatformerCharacter2D))]
public class Platformer2DUserControl : MonoBehaviour
{
    [HideInInspector] public PlatformerCharacter2D character;
    public Image gameWon;
    private bool jump;
    private bool airJump;
    private bool canJump;

    private void Awake()
    {
        character = GetComponent<PlatformerCharacter2D>();
        gameWon.enabled = false;
    }

    private void Update()
    {
        //on ground, not jumping - player can double jump now 
        if (character.isGrounded() && !jump)
        {
            jump = Input.GetButtonDown("Jump");
            canJump = true;
        }
        //player is midair - check if he can jump; if so, then calculate the air jump and adjust whether player can jump/not
        else if (!character.isGrounded())
        {
            //in air, check for airjump here
            if (canJump)
            {
                airJump = Input.GetButtonDown("Jump");
                if (airJump)
                    canJump = false;
            }
        }
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");


        //character.Move(h, jump);
        character.Move(h, jump, airJump);
        jump = false;
        airJump = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().tag == "EndFlag")
        {
            gameWon.enabled = true;
            Debug.Log("Game won!");
            StartCoroutine(ReloadGame());
        }
    }

    private IEnumerator ReloadGame()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Game");
    }
}

