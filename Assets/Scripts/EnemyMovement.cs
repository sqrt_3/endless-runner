﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyMovement : MonoBehaviour {

    public Collider2D leftBound;
    public Collider2D rightBound;
    private bool goingRight = false;
    public bool isStationary = false;


    private float killY = -12f;
    private float maxSpeed = 1f;
    private Rigidbody2D rb2D;
    private PlayerHealth health;

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        
    }

    private void FixedUpdate()
    {
        if (transform.position.y <= killY)
        {
            if (health)
                health.OnPlayerPush();
            Destroy(gameObject);
        }
            
        else if(!isStationary)
            Move(1f);
    }

    private void Move(float move)
    {
        if (goingRight)
            rb2D.velocity = new Vector2(move * maxSpeed, rb2D.velocity.y);
        else
            rb2D.velocity = new Vector2(move * maxSpeed * -1, rb2D.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            health = collision.collider.GetComponent<PlayerHealth>();
            if (collision.collider.transform.position.y > transform.position.y)
            {
                health.OnPlayerHeadshot();
                Destroy(gameObject);
            }
            else
            {
                health.OnPlayerDamage();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>() == leftBound)
        {
            goingRight = true;
        }
        else if (collision.GetComponent<Collider2D>() == rightBound)
        {
            goingRight = false;
        }
    }


}
