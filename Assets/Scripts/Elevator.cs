﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

    public Collider2D topBound;
    public Collider2D bottomBound;
    public bool goingUp = false;
    private float speed = 0.03f;

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        if (goingUp)
            transform.position = new Vector2(transform.position.x, transform.position.y + speed);
        else
            transform.position = new Vector2(transform.position.x, transform.position.y - speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Elevator collision with: " + collision.GetComponent<Collider2D>().tag);
        if (collision.GetComponent<Collider2D>() == topBound)
        {
            Debug.Log("Hit top!");
            goingUp = false;
        }
        else if (collision.GetComponent<Collider2D>() == bottomBound)
        {
            Debug.Log("Hit bottom!");
            goingUp = true;
        }
    }
}
