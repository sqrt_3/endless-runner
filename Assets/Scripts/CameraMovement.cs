﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public Transform target;
    
    private Vector2 moveVelocity;
    private float avgCameraYPos = 4.31f;
    private float minSize = 7.4f;
    

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        if((int)target.position.x % 4 == 0)
        {
            transform.position = Vector2.SmoothDamp(transform.position, new Vector2(target.position.x, avgCameraYPos), ref moveVelocity, 0.5f, 1f, 1f);
        }
    }
}
