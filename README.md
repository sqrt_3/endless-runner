# Endless Runner

Endless Runner 2D Game

##What's new?
* Only game scene is in game
* Full fledged UI system: keeps track of players headshots, pushes, time played, and health.
* Interactive damage system: if a player gets damaged by an enemy, he'll be pushed back, the camera will shake, and the background color will change according to current health.
* Music added into the game level
* Player will win the game if he successfully lands on the "end game" green flag
* Enemy AI system: AI is assigned routes that are bounded by 2 triggers - they will not fall unless the player pushes them.
* Camera will follow the player, but only every 4 or so in game units. There is a slow damp/moving of the camera to avoid stutter if player is moving too fast. 
* Camera mechanics also force players to be smart about taking breaks on specific points to be able to see the rest of the level.

####Credits
* Unity - software
* Super Mario - beach theme
* Matias Lago - game itself